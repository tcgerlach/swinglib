package com.talixa.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import com.talixa.swing.listeners.DefaultWindowListener;
import com.talixa.swing.shared.AppConstants;
import com.talixa.swing.shared.AppInfo;
import com.talixa.swing.shared.IconHelper;

public abstract class SwingApp {
	
	protected static JFrame frame;
	private static AppInfo appInfo;
	
	// keep only 1 so that it saves state
	public static final JFileChooser fileChooser = new JFileChooser();
	
	// To reference the frame in createAndShowGUI, it MUST be created first
	protected static void init(AppInfo appInfo) {
		SwingApp.appInfo = appInfo;
		
		// Create frame
		frame = new JFrame(appInfo.name + " - " + appInfo.version);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowListener(new DefaultWindowListener());
		frame.setLayout(new BorderLayout());
	}
	
	protected static void createAndShowGUI(JMenuBar menu, JToolBar tools) {
		// set icon
		IconHelper.setIcon(frame, appInfo.icon);		
	
		// add menus and toolbar
		if (menu != null) {
			frame.setJMenuBar(menu);
		}
		
		if (tools != null) {
			frame.add(tools, BorderLayout.NORTH);
		}
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(appInfo.width,appInfo.height));
		int left = (screenSize.width/2) - (appInfo.width/2);
		int top  = (screenSize.height/2) - (appInfo.height/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);				
	}	
	
	protected JMenuItem createMenuItem(String name, int mnemonic, ActionListener action) {
		JMenuItem menuItem = new JMenuItem(name);
		menuItem.setMnemonic(mnemonic);
		menuItem.addActionListener(action);
		return menuItem;
	}
	
	protected JButton createToolbarButton(String icon, String tooltip, ActionListener action) {
		JButton toolbtn = new JButton(IconHelper.getImageIcon(icon));
		toolbtn.setToolTipText(tooltip);
		toolbtn.addActionListener(action);
		return toolbtn;
	}

	public static void showInfoMessage(String title, String msg) {
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showErrorMessage(String msg, String fileName) {
		showErrorMessage(msg  + " : " + fileName);
	}
	
	public static void showErrorMessage(String msg) {
		JOptionPane.showMessageDialog(frame, msg, AppConstants.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
	}
}
