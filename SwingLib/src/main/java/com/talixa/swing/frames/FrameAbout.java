package com.talixa.swing.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.swing.listeners.ExitActionListener;
import com.talixa.swing.shared.AboutInfo;
import com.talixa.swing.shared.AppConstants;
import com.talixa.swing.shared.IconHelper;

public abstract class FrameAbout {
	
	private static final int BORDER = 10;
		
	public static void createAndShowGUI(JFrame owner, AboutInfo about) {
		// Create dialog
		JDialog dialog = new JDialog(owner, about.name + " " + about.version);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(dialog, about.icon);		
		
		// set escape listener
		addEscapeListener(dialog);
		
		// set modality
		dialog.setModal(true);
		
		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		holder.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		dialog.add(holder);
	
		// create components
		JLabel iconLabel = new JLabel(IconHelper.getImageIcon(about.icon));
		holder.add(iconLabel, BorderLayout.WEST);
		
		JLabel text = new JLabel(about.contents);
		Border padding = BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER);
		text.setBorder(padding);
		JButton ok = new JButton(AppConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));
				
		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(about.width,about.height));
		int left = (screenSize.width/2) - (about.width/2);
		int top  = (screenSize.height/2) - (about.height/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);						
	}	
	
	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
}
