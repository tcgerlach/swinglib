package com.talixa.swing.shared;

public class AppInfo {
	public String name;
	public String version;
	public String icon;
	public int width;
	public int height;
}
