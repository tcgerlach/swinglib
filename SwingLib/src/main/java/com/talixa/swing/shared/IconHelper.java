package com.talixa.swing.shared;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.ImageIcon;

public class IconHelper {

	private static ClassLoader cl = IconHelper.class.getClassLoader();	
	
	public static void setIcon(Window w, String icon) {
		Image im = Toolkit.getDefaultToolkit().getImage(cl.getResource(icon));
		w.setIconImage(im);
	}
	
	public static ImageIcon getImageIcon(String resource) {
		return new ImageIcon(cl.getResource(resource));
	}
}
