package com.talixa.swing.shared;

public class AppConstants {				
	// file menu
	public static final String MENU_FILE = "File";
	public static final String MENU_SAVE = "Save";
	public static final String MENU_OPEN = "Open";
	public static final String MENU_EXIT = "Exit";
	
	// help menu
	public static final String MENU_HELP = "Help";	
	public static final String MENU_ABOUT = "About";		
	
	public static final String LABEL_OK = "Ok";
	public static final String LABEL_CANCEL = "Cancel";
	
	public static final String ERROR_TITLE = "Error";
}
